import { Typography } from '@mui/material'
import { PieChart } from 'react-minimal-pie-chart'

import { useStore } from '../../store'

export const RankedItem = ({
    rank,
    topic,
    toxicityScore,
}: {
    rank: number
    topic: string
    toxicityScore: number
}) => {
    const { rankByLabel } = useStore()

    return (
        <div
            style={{
                width: '100%',
                display: 'flex',
                justifyContent: 'space-evenly',
                padding: '20px',
                borderBottom: '1px solid #ccc',
            }}
        >
            <div>
                <Typography variant="body1">
                    Rank: {rank}
                    <br />
                    Topic: {topic}
                    <br />
                    <div style={{ color: '#93c47d' }}>
                        Normal Tweets: {100 - toxicityScore}%
                    </div>
                    <div style={{ color: '#e06666' }}>
                        {rankByLabel} Tweets: {toxicityScore}%
                    </div>
                </Typography>
            </div>
            <div>
                <PieChart
                    style={{ height: '100px' }}
                    data={[
                        {
                            title: 'normal tweets',
                            value: 100 - toxicityScore,
                            color: '#93c47d',
                        },
                        {
                            title: `${rankByLabel} tweets`,
                            value: toxicityScore,
                            color: '#e06666',
                        },
                    ]}
                />
            </div>
        </div>
    )
}

import { useEffect, useState } from 'react'
import { Typography } from '@mui/material'

import { useStore } from '../../store'

type DataType = {
    primaryTopic: string
    primaryTopicSentiment: number
    isPerfectSentiment: boolean
    isMultipleTopics: boolean
    otherTopicsList: string
    highestRankedTopic: string
    secondRankedTopic: string
    highestRankedSentiment: number
    secondRankedSentiment: number
    isPrimaryTopicRank1: boolean
    isRank1Tie: boolean
}

export const Summary = ({
    toxicityScores,
}: {
    toxicityScores: [string, number][]
}) => {
    const { rankByLabel, topics } = useStore()
    const [data, setData] = useState<DataType>({
        primaryTopic: '',
        primaryTopicSentiment: 0,
        isPerfectSentiment: false,
        isMultipleTopics: false,
        otherTopicsList: '',
        highestRankedTopic: '',
        secondRankedTopic: '',
        highestRankedSentiment: 0,
        secondRankedSentiment: 0,
        isPrimaryTopicRank1: false,
        isRank1Tie: false,
    })

    useEffect(() => {
        setData({
            primaryTopic: topics?.[0],
            primaryTopicSentiment:
                toxicityScores?.find(
                    ([searchTopic, toxicityScore]) =>
                        data?.primaryTopic === searchTopic
                )?.[1] ?? 0,
            isPerfectSentiment: data?.primaryTopicSentiment === 0,
            isMultipleTopics: topics?.length > 1,
            otherTopicsList: topics?.slice(1)?.join(' and '),
            highestRankedTopic: toxicityScores?.[0]?.[0],
            secondRankedTopic: toxicityScores?.[1]?.[0],
            highestRankedSentiment: toxicityScores?.[0]?.[1],
            secondRankedSentiment: toxicityScores?.[1]?.[1],
            isPrimaryTopicRank1:
                data?.highestRankedTopic === data?.primaryTopic,
            isRank1Tie: toxicityScores?.[0]?.[1] === toxicityScores?.[1]?.[1],
        })
        console.log(data)
    }, [topics, toxicityScores, data])

    return toxicityScores.length > 0 ? (
        <Typography variant="body1">
            <div
                style={{
                    paddingBottom: '20px',
                    borderBottom: '1px solid #ccc',
                }}
            >
                <div style={{ textAlign: 'center', fontWeight: 'bold' }}>
                    Summary:
                </div>
                <div
                    style={{
                        display: 'flex',
                        flexDirection: 'column',
                        justifyContent: 'center',
                        borderTop: '1px solid #000',
                        borderLeft: '1px solid #000',
                        borderRight: '1px solid #000',
                        width: '95%',
                        margin: 'auto',
                    }}
                >
                    {[['Topic', 0], ...toxicityScores]?.map(
                        ([topic, toxicityScore], i) => (
                            <div
                                key={topic}
                                style={{
                                    display: 'flex',
                                    borderBottom: '1px solid #000',
                                }}
                            >
                                <div
                                    style={{
                                        width: '200px',
                                        borderRight: '1px solid #000',
                                        paddingLeft: '5px',
                                        ...(i === 0 && {
                                            fontWeight: 'bold',
                                        }),
                                    }}
                                >
                                    {topic}
                                </div>
                                <div
                                    style={{
                                        width: '100%',
                                        ...(i === 0
                                            ? { fontWeight: 'bold' }
                                            : {
                                                  backgroundColor: '#93c47d',
                                              }),
                                    }}
                                >
                                    <div
                                        style={{
                                            width: `${toxicityScore}%`,
                                            display: 'flex',
                                            flexDirection: 'column',
                                            backgroundColor: '#e06666',
                                        }}
                                    >
                                        {i === 0 ? (
                                            <div
                                                style={{
                                                    paddingLeft: '5px',
                                                    display: 'flex',
                                                }}
                                            >
                                                {i === 0 && 'Results'}
                                            </div>
                                        ) : (
                                            <span>&nbsp;</span>
                                        )}
                                    </div>
                                </div>
                            </div>
                        )
                    )}
                </div>
                {data.isMultipleTopics && ( // only show if more than one topic
                    <div style={{ padding: '20px', textAlign: 'center' }}>
                        {data.isPerfectSentiment ? ( // if sentiment is perfect
                            <>
                                Public sentiment towards {data.primaryTopic} is
                                perfect (0%) and does not currently require a
                                decrease in {rankByLabel}.
                            </>
                        ) : data.isPrimaryTopicRank1 ? ( // if first topic is highest rank
                            data.isRank1Tie ? ( // if first topic is tied for highest rank
                                <>
                                    Public sentiment towards {data.primaryTopic}{' '}
                                    is equal to the next highest competitor (
                                    {data.secondRankedTopic}). It does not
                                    currently require a decrease in{' '}
                                    {rankByLabel}, however, a decrease would put{' '}
                                    {data.primaryTopic} ahead.
                                </>
                            ) : (
                                // if first topic is not tied for highest rank
                                <>
                                    Public sentiment towards {data.primaryTopic}{' '}
                                    is greater than the next highest competitor
                                    ({data.secondRankedTopic}). It does not
                                    currently require a decrease in{' '}
                                    {rankByLabel}, however, a decrease by{' '}
                                    {data.primaryTopicSentiment} would make the
                                    public sentiment perfect.
                                </>
                            )
                        ) : (
                            // if first topic is not top
                            <>
                                In order to improve the public sentiment toward{' '}
                                {data.primaryTopic} when compared to{' '}
                                {data.otherTopicsList}, it would require a
                                decrease in {rankByLabel} by at least{' '}
                                {data.primaryTopicSentiment -
                                    data.highestRankedSentiment}
                                % to be above the highest ranked topic (
                                {data.highestRankedTopic}).
                            </>
                        )}
                    </div>
                )}
            </div>
        </Typography>
    ) : null
}

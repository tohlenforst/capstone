import { useEffect, useState } from 'react'
import * as toxicity from '@tensorflow-models/toxicity'

import { Summary } from './Summary'
import { RankedItem } from './RankedItem'
import { useStore } from '../../store'

export const RankingsDisplay = () => {
    const {
        certaintyThreshold,
        performScan,
        maxResults,
        rankByLabel,
        setPerformScan,
        topics,
    } = useStore()
    const [tweets, setTweets] = useState<Record<string, string[]>>({})
    const [toxicityScores, setToxicityScores] = useState<
        Record<string, number>
    >({})

    useEffect(() => {
        const fetchTweets = async () => {
            const newTweets = await topics.reduce(
                async (allNewTweets, topic) => {
                    const response = await fetch(
                        `/api/twitter?topic=${topic}&max_results=${String(
                            maxResults
                        )}`
                    )
                    const json = await response.json()
                    return json.error
                        ? { ...(await allNewTweets) }
                        : {
                              ...(await allNewTweets),
                              [topic]: json.data?.map(
                                  (tweet: Record<string, string>) => tweet.text
                              ),
                          }
                },
                {}
            )

            setTweets(newTweets)
        }
        if (topics.length > 0 && performScan) {
            setTweets({})
            fetchTweets()
        }
    }, [topics, maxResults, performScan])

    useEffect(() => {
        const getToxicityScores = async () => {
            const model = await toxicity.load(parseInt(certaintyThreshold), [
                rankByLabel,
            ])
            const newToxicityScores = await Object.entries(tweets)?.reduce(
                async (allNewToxicityScores, [topic, topicTweets]) => {
                    const predictions = await model?.classify(topicTweets)
                    const results = predictions[0].results
                    const totalMatched = results.filter(
                        prediction => prediction.match
                    ).length

                    return {
                        ...(await allNewToxicityScores),
                        [topic]: (totalMatched / results.length) * 100,
                    }
                },
                {}
            )
            console.log('newToxicityScores', newToxicityScores)
            setToxicityScores(newToxicityScores)
            setPerformScan(false)
        }
        if (Object.entries(tweets).length > 0 && performScan) {
            setToxicityScores({})
            getToxicityScores()
        }
    }, [rankByLabel, certaintyThreshold, tweets, performScan, setPerformScan])

    const simplifiedToxicityScores = (toxicityScores: Record<string, number>) =>
        Object.entries({ ...toxicityScores })?.sort(
            ([topicA, toxicityScoreA], [topicB, toxicityScoreB]) => {
                if (toxicityScoreA < toxicityScoreB) return -1
                if (toxicityScoreA > toxicityScoreB) return 1
                return 0
            }
        )

    return (
        <div
            style={{
                width: '100%',
                display: 'flex',
                flexDirection: 'column',
                gap: '10px',
            }}
        >
            <Summary
                toxicityScores={simplifiedToxicityScores(toxicityScores)}
            />
            {simplifiedToxicityScores(toxicityScores)?.map(
                ([topic, toxicityScore], index) => (
                    <RankedItem
                        key={topic}
                        rank={index + 1}
                        topic={topic}
                        toxicityScore={toxicityScore}
                    />
                )
            )}
        </div>
    )
}

export default RankingsDisplay

import { Sidebar } from './Sidebar'
import { RankingsDisplay } from './Content'

export const Main = () => (
    <div style={{ display: 'flex' }}>
        <Sidebar />
        <RankingsDisplay />
    </div>
)

export default Main

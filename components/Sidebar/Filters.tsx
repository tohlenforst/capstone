import {
    FormControl,
    InputLabel,
    MenuItem,
    Select,
    TextField,
    Typography,
} from '@mui/material'
import { useStore } from '../../store'

export const Filters = () => {
    const {
        certaintyThreshold,
        rankByLabel,
        maxResults,
        setCertaintyThreshold,
        setRankByLabel,
        setMaxResults,
    } = useStore()
    const labels = [
        'identity_attack',
        'insult',
        'obscene',
        'severe_toxicity',
        'sexual_explicit',
        'threat',
        'toxicity',
    ]

    return (
        <div>
            <Typography variant="h5">Filters:</Typography>
            <br />
            <FormControl fullWidth>
                <InputLabel id="rankByLabel-label">
                    Rank By Least Amount of
                </InputLabel>
                <Select
                    labelId="rankByLabel-label"
                    id="rankByLabel"
                    value={rankByLabel}
                    label="Rank By Least Amount of"
                    onChange={e => setRankByLabel(e.target.value)}
                >
                    {labels.map(label => (
                        <MenuItem value={label} key={label}>
                            {label}
                        </MenuItem>
                    ))}
                </Select>
            </FormControl>
            <br />
            <br />
            <TextField
                label="Certainty Threshold"
                value={certaintyThreshold}
                onChange={e => setCertaintyThreshold(e.target.value)}
                fullWidth
            />
            <br />
            <br />
            <TextField
                label="Maximum Tweet Results"
                value={maxResults}
                onChange={e => setMaxResults(e.target.value)}
                fullWidth
            />
        </div>
    )
}

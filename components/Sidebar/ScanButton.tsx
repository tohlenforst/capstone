import { Button, CircularProgress } from '@mui/material'

import { useStore } from '../../store'

export const ScanButton = () => {
    const { performScan, setPerformScan } = useStore()

    return (
        <div
            style={{
                display: 'flex',
                justifyContent: 'center',
                alignItems: 'center',
            }}
        >
            {performScan ? (
                <CircularProgress />
            ) : (
                <Button
                    variant="contained"
                    color="primary"
                    onClick={() => setPerformScan(true)}
                >
                    Scan
                </Button>
            )}
        </div>
    )
}

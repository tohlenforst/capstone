import { useState } from 'react'
import {
    FormControl,
    IconButton,
    InputAdornment,
    InputLabel,
    OutlinedInput,
    Typography,
} from '@mui/material'
import { Add, Delete } from '@mui/icons-material'
import { useStore } from '../../store'

export const Topics = () => {
    const { addTopic, removeTopicByIndex, topics, updateTopic } = useStore()
    const [newTopic, setNewTopic] = useState<string>('')

    const handleAddTopic = () => {
        addTopic(newTopic)
        setNewTopic('')
    }

    return (
        <div>
            <Typography variant="h5">Topics:</Typography>
            <br />
            {topics.map((topic, index) => (
                <FormControl
                    key={index}
                    variant="outlined"
                    style={{ marginBottom: '10px' }}
                    fullWidth
                >
                    <OutlinedInput
                        value={topic}
                        onChange={e => updateTopic(index, e.target.value)}
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    onClick={() => removeTopicByIndex(index)}
                                    edge="end"
                                >
                                    <Delete />
                                </IconButton>
                            </InputAdornment>
                        }
                        fullWidth
                    />
                </FormControl>
            ))}
            <FormControl variant="outlined" fullWidth>
                <InputLabel id="addTopic-label" htmlFor="addTopic">
                    Add Topic
                </InputLabel>
                <OutlinedInput
                    value={newTopic}
                    id="addTopic"
                    onChange={e => setNewTopic(e.target.value)}
                    endAdornment={
                        <InputAdornment position="end">
                            <IconButton onClick={handleAddTopic} edge="end">
                                <Add />
                            </IconButton>
                        </InputAdornment>
                    }
                    label="Add Topic"
                />
            </FormControl>
            <br />
        </div>
    )
}

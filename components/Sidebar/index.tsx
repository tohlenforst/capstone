import { Topics } from './Topics'
import { Filters } from './Filters'
import { ScanButton } from './ScanButton'

export const Sidebar = () => (
    <div
        style={{
            display: 'flex',
            flexDirection: 'column',
            gap: '20px',
            padding: '10px',
            minHeight: '100vh',
            borderRight: '1px solid #ccc',
            width: '300px',
        }}
    >
        <Topics />
        <Filters />
        <ScanButton />
    </div>
)

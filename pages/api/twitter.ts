import type { NextApiRequest, NextApiResponse } from 'next'
import fetch from 'node-fetch'

export const getTweets = async (topic: string, max_results: number) => {
    const request = await fetch(
        'https://api.twitter.com/2/tweets/search/recent' +
            `?query=${topic}` +
            '&tweet.fields=created_at' +
            `&max_results=${max_results}`,
        {
            method: 'GET',
            headers: {
                Authorization: process.env.TWITTER_BEARER_TOKEN ?? '',
                'Content-Type': 'application/json',
            },
        }
    )
    const json = await request.json()
    return json
}

const twitter = async (req: NextApiRequest, res: NextApiResponse) => {
    const { topic, max_results } = req.query
    const tweets = await getTweets(
        topic as string,
        parseInt(max_results as string)
    )
    res.status(200).json(tweets)
}

export default twitter

import type { NextPage } from 'next'
import dynamic from 'next/dynamic'
import Head from 'next/head'
import { useEffect } from 'react'

import { useStore } from '../store'

const Main = dynamic(() => import('../components/Main'), {
    ssr: false,
})

const Home: NextPage = () => {
    const { setPerformScan } = useStore()

    useEffect(() => setPerformScan(false), [setPerformScan])

    return (
        <div>
            <Head>
                <title>Capstone</title>
                <meta name="description" content="WGU Capstone Project" />
            </Head>
            <main>
                <Main />
            </main>
        </div>
    )
}

export default Home

import createStore from 'zustand'
import { persist } from 'zustand/middleware'

type Store = {
    topics: string[]
    certaintyThreshold: string
    rankByLabel: string
    maxResults: string
    performScan: boolean
    setTopics: (topics: string[]) => void
    addTopic: (topic: string) => void
    removeTopic: (topic: string) => void
    removeTopicByIndex: (index: number) => void
    updateTopic: (index: number, topic: string) => void
    setCertaintyThreshold: (certaintyThreshold: string) => void
    setRankByLabel: (rankByLabel: string) => void
    setMaxResults: (maxResults: string) => void
    setPerformScan: (performScan: boolean) => void
}

export const useStore = createStore(
    persist<Store>(
        set => ({
            topics: [],
            certaintyThreshold: '0.9',
            rankByLabel: 'toxicity',
            maxResults: '10',
            performScan: false,
            setTopics: topics => set(state => ({ ...state, topics })),
            addTopic: topic =>
                set(state => ({ ...state, topics: [...state.topics, topic] })),
            removeTopic: topic =>
                set(state => ({
                    ...state,
                    topics: state.topics.filter(t => t !== topic),
                })),
            removeTopicByIndex: index =>
                set(state => ({
                    ...state,
                    topics: state.topics.filter((_, i) => i !== index),
                })),
            updateTopic: (index, topic) =>
                set(state => ({
                    ...state,
                    topics: state.topics.map((t, i) =>
                        i === index ? topic : t
                    ),
                })),
            setCertaintyThreshold: certaintyThreshold =>
                set(state => ({
                    ...state,
                    certaintyThreshold,
                })),
            setRankByLabel: label =>
                set(state => ({ ...state, rankByLabel: label })),
            setMaxResults: maxResults =>
                set(state => ({ ...state, maxResults })),
            setPerformScan: performScan =>
                set(state => ({ ...state, performScan })),
        }),
        {
            name: 'capstone',
        }
    )
)
